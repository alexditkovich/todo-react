import React, {useState} from "react";
import {observer} from "mobx-react";

import {useStore} from "../stores/useStore";

export const Component = () => {
    const {globalStore} = useStore()
    const [editableTaskIndex, setEditableTaskIndex] = useState(null);
    const [editableTaskValue, setEditableTaskValue] = useState('');

    const onSubmit = index => {
        globalStore.editTask(index, editableTaskValue);
        setEditableTaskValue(null);
    }

    return (
        <div className="TasksBox">
            {globalStore.tasks.map((task, index) => (
                <div key={task.id}>
                    <input id={task.id} type="checkbox" onChange={() => globalStore.invertIsComplete(index)}/>

                    <label htmlFor={task.id}>
                        {task.title}
                    </label>

                    {editableTaskIndex === index && (
                        <form onSubmit={() => onSubmit(index)}>
                            <input value={editableTaskValue} onChange={e => setEditableTaskValue(e.target.value)}/>

                            <button type="submit">Edit</button>
                        </form>
                    )}

                    {editableTaskIndex !== index && <button onClick={() => setEditableTaskIndex(index)}>Edit</button>}

                    <button onClick={() => globalStore.deleteTask(index)}>Delete</button>
                </div>
            ))}
        </div>
    )
}
export const TasksBox = observer(Component);
