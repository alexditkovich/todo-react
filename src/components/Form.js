import React, {useState} from "react";
import {observer} from "mobx-react";

import {useStore} from "../stores/useStore";

export const Component = () => {
    const [newTaskValue, setNewTaskValue] = useState('');
    const {globalStore} = useStore();

    const onSubmit = e => {
        e.preventDefault();

        globalStore.pushToTasks({
            title: newTaskValue,
            isComplete: false,
            id: globalStore.tasks.length + 1
        });
        setNewTaskValue('');
    };

    return (
        <form onSubmit={onSubmit}>
            <input value={newTaskValue} onChange={e => setNewTaskValue(e.target.value)}/>
            <button type='submit'>Add</button>
        </form>
    )
}

export const Form = observer(Component);
