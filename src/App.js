import React from "react";

import {Form} from "./components/Form";
import {TasksBox} from "./components/TasksBox";

import {GlobalStore} from "./stores/GlobalStore";
import {StoreProvider} from "./stores/useStore";

const stores = {
    globalStore: new GlobalStore()
};

const App = () => (
    <StoreProvider store={stores}>
        <div className="App">
            <Form />

            <TasksBox />
        </div>
    </StoreProvider>
);

export default App;
