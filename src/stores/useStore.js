import React from 'react';

const StoreContext = React.createContext();

export const StoreProvider = ({ children, store }) => (
    <StoreContext.Provider value={store}>
        {children}
    </StoreContext.Provider>
);

export const useStore = () => React.useContext(StoreContext);
