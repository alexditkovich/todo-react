import {makeAutoObservable} from "mobx";

import {deviceStorage, TASKS} from "../extra/deviceStorage";

export class GlobalStore {
    constructor() {
        makeAutoObservable(this);
    }

    pushToTasks(task) {
        this.tasks.push(task);
        deviceStorage.set(TASKS, this.tasks);
    }

    invertIsComplete(index) {
        this.tasks[index].isComplete = !this.tasks[index].isComplete;
        deviceStorage.set(TASKS, this.tasks);
    }

    deleteTask(index) {
        this.tasks.splice(index, 1);
        deviceStorage.set(TASKS, this.tasks);
    }

    editTask(index, title) {
        this.tasks[index].title = title;
        deviceStorage.set(TASKS, this.tasks);
    }

    tasks = deviceStorage.get(TASKS) ? deviceStorage.get(TASKS) : [];
}
